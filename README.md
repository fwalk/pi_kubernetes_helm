Role Name
=========

Ansible role to install Kubernetes helm and tiller on a Raspberry-Pi cluster.

Requirements
------------

Working kubernetes cluster.

Right now tested only with Raspbian.

Role Variables
--------------

| Name         | Default                            |
|--------------|------------------------------------|
| helm_version | 2.12.0                             |
| tiller_image | jessestuart/tiller:v{helm_version} |

Dependencies
------------

No other roles required.

Example Playbook
----------------

```yaml
- hosts:
    - <host>

  roles:
    - role: pi_kubernetes_helm
      vars:
        helm_version: 2.12.1
```

```shell
$ helm init --tiller-image=jessestuart/tiller:v2.12.1
Creating /home/pirate/.helm
Creating /home/pirate/.helm/repository
Creating /home/pirate/.helm/repository/cache
Creating /home/pirate/.helm/repository/local
Creating /home/pirate/.helm/plugins
Creating /home/pirate/.helm/starters
Creating /home/pirate/.helm/cache/archive
Creating /home/pirate/.helm/repository/repositories.yaml
Adding stable repo with URL: https://kubernetes-charts.storage.googleapis.com
Adding local repo with URL: http://127.0.0.1:8879/charts
$HELM_HOME has been configured at /home/pirate/.helm.

Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
To prevent this, run `helm init` with the --tiller-tls-verify flag.
For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation
Happy Helming!
```

Author Information
------------------

fwalk___gitlab

References
----------

- https://docs.helm.sh/using_helm/#quickstart-guide
- https://www.scottietse.com/2018/06/01/Install-helm-on-raspberry-Pi-(ARM-device)/
- https://hub.docker.com/r/jessestuart/tiller/
